const car = (name, model, owner, phone, image, year) => ({name, model, owner, phone, image, year})

const cars = [
    car('Ford', 'Focus', 'Max', '+9070000007', 'img/ford.jpg', '2016'),
    car('Ford', 'Mondeo', 'Vla', '+9070000008', 'img/mondeo.jpg', '2018'),
    car('Porshe', 'Panamera', 'Irina', '+9070000009', 'img/panamera.jpg', '2019')
]

new Vue({
    el: '#container',
    data: {
        cars: cars,
        car: cars[0],
        selectedCarIndex: 0,
        phoneVisibility: false,
        search: '',
        modelVisibility: false
    },
    methods: {
        selectCar(index) {
            console.log('click', index)
            this.car = cars[index]
            this.selectedCarIndex = index
        }
    },
    computed: {
        phoneBtnText(){
            return this.phoneVisibility ? 'Show phone' : 'Hide phone'
        },

        filteredCars() {
            return this.cars.filter(car => {
                return car.name.indexOf(this.search) > -1 || car.model.indexOf(this.search) > -1
            })
        }
    }

})